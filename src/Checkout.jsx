import React, { useEffect, useState } from "react";
import {Link} from "react-router-dom"
import useFetchAll from "./services/useFetchAll";
import { useCart } from "./cartContext";
import Spinner from "./Spinner";
import { getShippingAddress, saveBill } from "./services/shippingService";
import useFetch from "./services/useFetch";

const STATUS ={
  IDLE: "IDLE",
  SUBMITTED: "SUBMITTED",
  SUBMITTING:"SUBMITTING",
  COMPLETED: "COMPLETED"
}
export default function Checkout() {
  const {cart,dispatch} = useCart()
  const [address,setAddress] = useState()
  const [touched,setTouched] =  useState({})
  const [status,setStatus] = useState(STATUS.IDLE)
  const [submitError,setSubmitError] = useState()

  const errors = getError(cart,address)
  const isValid = (Object.keys(errors).length===0)

  const urls = cart.map((i) => `products/${i.id}`);
  
  const { data: listAddress} = useFetch("shippingAddress")

  const { data: products, loading, error} =useFetchAll(urls);
  
  function renderItem(itemInCart) {
    const { id, sku, quantity } = itemInCart;

    if(!products) return <li key={sku}></li>

    const { price, name, image, skus } = products.find(
      (p) => p.id === parseInt(id)
    );
    const { size } = skus.find((s) => s.sku === sku);
    
    return (
      <li key={sku} className="cart-item">
        <img src={`/images/${image}`} alt={name} />
        <div>
          <h3>{name}</h3>
          <p>${price}</p>
          <p>Size: {size}</p>
          <p>
            <select
              id ="cart"
              aria-label={`Select quantity for ${name} size ${size}`}
              onChange={(e) => dispatch({type:"updateQuantity",sku, quantity:parseInt(e.target.value)})}
              onBlur={handleBlur}
              value={quantity}
            >
              <option value="0">Remove</option>
              <option value="1">1</option>
              <option value="2">2</option>
              <option value="3">3</option>
              <option value="4">4</option>
              <option value="5">5</option>
            </select>
          </p>
        </div>
      </li>
    );
  }
  //handle functions
  function handleOnChangeAddress(e){
    e.persist()
    setAddress(parseInt(e.target.value))
  }
  function handleBlur(e){
    e.persist()
    setTouched((cur)=>{
      return {...cur,[e.target.id]:true}
    })
  }
  function handleSubmit(e){
    e.preventDefault()
    if(isValid){
      setStatus(STATUS.SUBMITTING)
      try {
        saveBill(address,cart)
        dispatch({type:'empty'})
        setStatus(STATUS.COMPLETED)
      } catch (error) {
        setSubmitError(error)
      }
    }else{
      setStatus(STATUS.SUBMITTED)
    }
  }
  //get errors 
  function getError(cart,address){
    let result = {}
    if(cart.length===0) result.cart = 'cart is empty'
    if(!address) result.address = 'address is required'
    return result
  }

  if (loading) return <Spinner />;
  if (error) throw error;
  if(submitError) throw submitError
  if(status === STATUS.COMPLETED) return <h1>Thanks for shopping!</h1>
  const numItemsInCart = cart.reduce((total,item)=> total+item.quantity,0)
  let costOfCart = 0;
  if (products) costOfCart = cart.reduce((total,item)=>total+(products.find(i => i.id===parseInt(item.id)).price*item.quantity),0)
  return (
    <>
      <h1>Check Out</h1>
      {!isValid && status=== STATUS.SUBMITTED && (
        <div role="alert">
            <p>Please fix the flowwing errors</p>
            <ul>
              {Object.keys(errors).map((key)=>
                <li key={key}>{errors[key]}</li>)}
            </ul>
        </div>
      )}
      <form onSubmit={handleSubmit}>
        <div>
          <label htmlFor="products">Products</label>
          <br />
          <section id="cart">
            {numItemsInCart>0?`${numItemsInCart} items in cart`:`No items in cart`}
            <ul>{cart.map(renderItem)}</ul>
          </section>
        </div>
        <div>
          <h2>Cost: {costOfCart}$</h2>
        </div>
        <div>
          <label htmlFor="address">Address</label>
          <br />
          <select
            id="address"
            value={(listAddress && address) ? listAddress.find(i=>i.id===address).id : 0}
            onChange={handleOnChangeAddress}
            onBlur={handleBlur}
          >
            <option value={0}>Select Country</option>
            {listAddress.map((a)=><option key={a.id} value={a.id}>{a.city+", "+a.country}</option>)}
          </select>
          <br/>
          <Link to={"/saveAddress"}><button className="btn btn-primary">Add New Address</button></Link>
        </div>
        <div>
          <input
            type="submit"
            className="btn btn-primary"
            value="check out"
          />
        </div>
      </form>
      
    </>
  );
}
