export default function cartReducer(cart,action){
    
    switch(action.type){
        case "empty":
            return []
        case "add":
            const {id,sku} = action
            const itemInCart = cart.find((i)=> i.sku===sku)
            if (itemInCart){
                return cart.map((i)=> i.sku===sku?{...i, quantity: i.quantity+1}:i)
            }else{
                return [...cart,{id,sku,quantity:1}]
            }
        case "updateQuantity":{
            const {sku,quantity} = action
            return quantity > 0 ? 
            cart.map((i)=> i.sku===sku?{...i, quantity: quantity}:i):
            cart.filter((i)=> i.sku!==sku)
        }
        default:
            throw Error('unhandle action'+action.type)
    }
}