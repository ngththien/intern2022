import React from "react";
import "./App.css";
import Footer from "./Footer";
import Header from "./Header";
import Products from "./Products";
import Detail from "./Detail";
import Cart from "./Cart"
import { Routes,Route } from "react-router-dom";
import Checkout from "./Checkout"
import SaveAddress from "./SaveAddress";
import Bill from "./Bill";

export default function App() {
  
  return (
    <>
      <div className="content">
        <Header />
        <main>
          <Routes>
            <Route path="/" element={<h1>Wellcome</h1>} />
            <Route path="/:category" element={<Products/>} />
            <Route path="/:category/:id" element={<Detail/>}  />
            <Route path="/cart" element={<Cart/>} />
            <Route path="/checkout" element={<Checkout  />} />
            <Route path="/saveAddress" element={<SaveAddress  />} />
            <Route path="/bill" element={<Bill  />} />
          </Routes>
        </main>
      </div>
      <Footer />
    </>
  );
}
