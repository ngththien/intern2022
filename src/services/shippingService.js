const baseUrl = process.env.REACT_APP_API_BASE_URL;

export async function getShippingAddressById(userId) {
  return fetch(baseUrl + "shippingAddress/" + userId).then((response) => {
    if (response.ok) return response.json();
    throw response;
  });
}

export async function getShippingAddress() {
  return fetch(baseUrl + "shippingAddress").then((response) => {
    if (response.ok) return response.json();
    throw response;
  });
}

export async function saveShippingAddress(address) {
  return fetch(baseUrl + "shippingAddress", {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify(address),
  });
}

export async function saveBill(idAddress,cart) {
  const bill = {
    idAddress,
    cart
  }
  return fetch(baseUrl + "bill", {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify(bill),
  });
}
