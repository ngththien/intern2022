import { useEffect, useState } from "react";
import useFetch from "./useFetch";
import useFetchAll from "./useFetchAll";

const baseUrl = process.env.REACT_APP_API_BASE_URL;

export default  function useBill(){
    const [error,setError] = useState(null)
    const [loading,setLoading] = useState(true)
    
    const [bills,setBills] = useState([])

    useEffect(()=>{
        async function getBills(){
            try {
                const response = await fetch(process.env.REACT_APP_API_BASE_URL + "bill");
                if (response.ok){
                    let json = await response.json()
                    let newBills = []
                    for (const bill of json){
                        let newBill = {}
                        let newCart = []
                        let address =""
                        const res = await fetch(process.env.REACT_APP_API_BASE_URL+"shippingAddress/"+bill.idAddress)
                        if(res.ok){
                            let json = await res.json()
                            address = json.city+"-"+json.country
                        }else setError(res)
                        for (const item of bill.cart){
                            let newItem = {}
                            const response = await fetch(process.env.REACT_APP_API_BASE_URL+"products/"+item.id)
                            if(response.ok){
                                let json = await response.json()
                                newItem = {...item,name: json.name,image: json.image,price: json.price}
                            }else{
                                setError(response)
                            }
                            newCart.push(newItem)
                        }
                        newBill= {...bill,cart: newCart,address: address}
                        newBills.push(newBill)
                    }
                    setBills(newBills)
                }else throw response
            } catch (error) {
                setError(error)
            }finally{
                setLoading(false)
            }
        }
        getBills()
    },[])
    return {
        bills,loading,error
    }
}