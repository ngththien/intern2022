import React, { useEffect, useState } from "react";
import useBill from "./services/useBill";
import Spinner from "./Spinner";

export default function Bill(){
    const {bills,loading,error} = useBill()
    
    //render
    function renderItem(item){
        return (
            <li key={item.id} className="cart-item">
                <img src={`/images/${item.image}`} alt={item.name} />
                <div>
                    <h3>name: {item.name}</h3>
                    <p>price: ${item.price}</p>
                    <p>quantity: {item.quantity}</p>
                </div>
            </li>
        )
    }
    function render(bill){
        return (
            <section key={bill.id} id="cart">
                <ul>
                    <h2>to: {bill.address}</h2>
                    {bill.cart.map(renderItem)}
                </ul>
            </section>
            
        )
    }
    if(loading) return <Spinner/>
    if(error) throw error
    return(
        <>
            {bills.map(render)}
        </>
    )
}