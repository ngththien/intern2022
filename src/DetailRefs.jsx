import React,{useRef} from "react";
import { useContext } from "react";
import { useParams,useNavigate } from "react-router-dom";
import PageNotFound from "./PageNotFound";
import useFetch from "./services/useFetch";
import Spinner from "./Spinner";
import { CartContext } from "./cartContext";
export default function Detail() {
  const {id} = useParams()
  const {data: product,error,loading} = useFetch('products/'+id)
  const navigate = useNavigate()
  const {dispatch} = useContext(CartContext)
  const skuRef = useRef()
  // TODO: Display these products details
  if (loading) return <Spinner></Spinner>
  if (!product) return <PageNotFound/>
  if (error) throw error
  return (
      <div id="detail">
        <h1>{product.name}</h1>
        <p>{product.description}</p>
        <p id="price">${product.price}</p>
        
        <select id="size" ref={skuRef} 
            >
               <option value="">What size?</option>
              {product.skus.map((s)=> <option key={s.sku} value={s.sku}>{s.size}</option>)}
            </select>
          <p>
            <button  className="btn btn-primary" 
            onClick={()=>{
              const sku = skuRef.current.value
              dispatch({type:"add",id,sku})
              navigate("/cart")
            }}>Add to cart</button>
          </p> 
        
        <img src={`/images/${product.image}`} alt={product.category} />
      </div>
  );
}
