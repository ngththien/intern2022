import React,{useReducer,useEffect, useContext} from "react";
import cartReducer from "./cartReducer"

const CartContext = React.createContext(null)

let initCart;
try {
  initCart = JSON.parse(localStorage.getItem('cart')) ?? []
} catch (error) {
  initCart = []
}

export default function CartProvider(props){
    const [cart,dispatch] = useReducer(cartReducer,initCart)

    useEffect(()=>localStorage.setItem('cart', JSON.stringify(cart)),[cart])
    const contextValue = {
        cart,
        dispatch
    }
    return(
        <CartContext.Provider value={ contextValue}>
            {props.children}
        </CartContext.Provider>
    )
}
export function useCart(){
    const context = useContext(CartContext);
    return context
}